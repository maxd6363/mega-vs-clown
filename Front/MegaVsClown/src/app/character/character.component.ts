import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import Character from 'src/models/Character';
import { loadingCharacter } from '../global';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css'],
})
export class CharacterComponent implements OnInit {
  @Output() onCharacterAdded: EventEmitter<Character> = new EventEmitter();

  @Input() character: Character = loadingCharacter;
  @Input() addButton: boolean = true;

  constructor() {}

  ngOnInit(): void {
    if (this.character == loadingCharacter) this.addButton = false;
  }

  addCharacterClicked(): void {
    this.onCharacterAdded.emit(this.character);
  }
}
