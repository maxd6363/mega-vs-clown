'use stric';

import Arena from 'src/models/Arena';
import Character from 'src/models/Character';
import Attack from 'src/models/Attack';

//export const API_PATH = 'https://localhost:7117';
export const API_PATH = 'http://91.170.211.147:57000';
export let playerName = '';

export const setPlayerName = (name: string) => {
  playerName = name;
};

export let gameCode = '';
export const setGameCode = (code: string) => {
  gameCode = code;
};

export const loadingCharacter: Character = new Character(
  'Loading',
  0,
  0,
  'https://i.ibb.co/xfD2Nqd/loading.gif',
  'https://i.ibb.co/xfD2Nqd/loading.gif',
  []
);

export const choosingCharacter: Character = new Character(
  'Choose',
  0,
  0,
  'https://i.ibb.co/4R30nt0/question-Mark.png',
  'https://i.ibb.co/4R30nt0/question-Mark.png',
  []
);
export const deadCharacter: Character = new Character(
  'KO',
  0,
  0,
  'https://i.ibb.co/4R30nt0/question-Mark.png',
  'https://i.ibb.co/rvNjyPZ/ezgif-com-gif-maker.gif',
  []
);

export const loadingArena: Arena = new Arena(
  'Loading arena...',
  'https://i.ibb.co/2spkLxB/arena1.gif'
);

export const loadingAttack: Attack = new Attack(
  -1,
  'Loading',
  'Loading',
  0,
  0,
  0
);
export const chooseAttack: Attack = new Attack(-1, 'Choisir...', '', 0, 0, 0);
export const fourAttacks: Attack[] = [
  chooseAttack,
  chooseAttack,
  chooseAttack,
  chooseAttack,
];

export let selectedArena = loadingArena;
export const setSelectedArena = (arena: Arena) => {
  selectedArena = arena;
};
