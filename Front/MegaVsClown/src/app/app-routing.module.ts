import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { ArenaChooserComponent } from './arena-chooser/arena-chooser.component';
import { ArenaComponent } from './arena/arena.component';
import { CharacterChooserComponent } from './character-chooser/character-chooser.component';
import { JoinComponent } from './join/join.component';

const routes: Routes = [
  { path: '', component: JoinComponent },
  { path: 'selection', component: CharacterChooserComponent },
  { path: 'arenaSelection', component: ArenaChooserComponent },
  { path: 'fight', component: ArenaComponent },
  { path: 'admin', component: AdminComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
