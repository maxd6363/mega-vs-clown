import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import {
  NgbPaginationModule,
  NgbAlertModule,
} from '@ng-bootstrap/ng-bootstrap';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CharacterComponent } from './character/character.component';
import { CharacterChooserComponent } from './character-chooser/character-chooser.component';
import { JoinComponent } from './join/join.component';
import { ArenaComponent } from './arena/arena.component';
import { ArenaChooserComponent } from './arena-chooser/arena-chooser.component';
import { CharacterIngameComponent } from './character-ingame/character-ingame.component';
import { AdminComponent } from './admin/admin.component';
import { CharacterAdminComponent } from './character-admin/character-admin.component';
import { CharacterAddAdminComponent } from './character-add-admin/character-add-admin.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CharacterComponent,
    CharacterChooserComponent,
    JoinComponent,
    ArenaComponent,
    ArenaChooserComponent,
    CharacterIngameComponent,
    AdminComponent,
    CharacterAdminComponent,
    CharacterAddAdminComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbPaginationModule,
    NgbAlertModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
