import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import Character from 'src/models/Character';
import Player from 'src/models/Player';
import { API_PATH, gameCode, playerName, setPlayerName } from './global';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  constructor(private http: HttpClient) {}

  createGame(): Observable<string> {
    const headers = new HttpHeaders().set(
      'Content-Type',
      'text/plain; charset=utf-8'
    );
    console.log('creating game');
    //console.log(`${API_PATH}/Game/CreateGame`);
    return this.http.post(`${API_PATH}/Game/CreateGame`, [], {
      responseType: 'text',
    });
  }

  isGameExisting(): Observable<boolean> {
    console.log(`is game ${gameCode} existing game`);
    //console.log(`${API_PATH}/Game/IsGameExisting?code=${gameCode}`);
    return this.http.get<boolean>(
      `${API_PATH}/Game/IsGameExisting?code=${gameCode}`
    );
  }

  addPlayer(name: string): Observable<any> {
    console.log(`adding player ${name}`);
    //console.log(
    //  `${API_PATH}/Game/AddPlayer?code=${gameCode}&name=${playerName}`
    //);
    setPlayerName(name);
    return this.http.post(
      `${API_PATH}/Game/AddPlayer?code=${gameCode}&name=${playerName}`,
      [],
      {
        observe: 'response',
      }
    );
  }

  startGame(): Observable<any> {
    console.log(`starting game`);
    //console.log(`${API_PATH}/Game/StartGame?code=${gameCode}&`);
    return this.http.put(`${API_PATH}/Game/StartGame?code=${gameCode}`, []);
  }

  stopGame() {
    console.log(`stopping game`);
    //console.log(`${API_PATH}/Game/StopGame?code=${gameCode}`);
    this.http
      .put(`${API_PATH}/Game/StopGame?code=${gameCode}&`, [])
      .subscribe();
  }

  addCharacter(character: Character) {
    console.log(`adding character ${character.characterName} to ${playerName}`);
    //console.log(
    //  `${API_PATH}/Game/AddCharacter?code=${gameCode}&playerName=${playerName}&characterName=${character.characterName}`
    //);
    this.http
      .post(
        `${API_PATH}/Game/AddCharacter?code=${gameCode}&playerName=${playerName}&characterName=${character.characterName}`,
        []
      )
      .subscribe();
  }

  getPlayers(): Observable<Player[]> {
    console.log(`getting players`);
    //console.log(`${API_PATH}/Game/GetPlayers?code=${gameCode}`);
    return this.http.get<Player[]>(
      `${API_PATH}/Game/GetPlayers?code=${gameCode}`
    );
  }

  makeMove(attackName: string): Observable<any> {
    console.log(`making move from ${playerName} with ${attackName}`);
    //console.log(
    //  `${API_PATH}/Game/MakeMove?code=${gameCode}&attackerPlayerName=${playerName}&attackName=${attackName}`
    //);
    return this.http.post(
      `${API_PATH}/Game/MakeMove?code=${gameCode}&attackerPlayerName=${playerName}&attackName=${attackName}`,
      []
    );
  }

  getWinner(): Observable<string> {
    console.log(`getting winnger`);
    const headers = new HttpHeaders().set(
      'Content-Type',
      'text/plain; charset=utf-8'
    );
    const requestOptions: Object = {
      headers: headers,
      responseType: 'text',
    };
    //console.log(`${API_PATH}/Game/GetWinner?code=${gameCode}`);
    return this.http.get<string>(
      `${API_PATH}/Game/GetWinner?code=${gameCode}`,
      requestOptions
    );
  }
}
