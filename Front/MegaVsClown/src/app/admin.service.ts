import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_PATH } from './global';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  addCharacter(name: string, hp: number, defense: number, sprite: string, fullsprite: string, attack1: number, attack2: number, attack3: number, attack4: number) {
    return this.http.post(`${API_PATH}/Admin/AdminAddCharacter?name=${name}&hp=${hp}&defense=${defense}&sprite=${sprite}&fullsprite=${fullsprite}&attack1=${attack1}&attack2=${attack2}&attack3=${attack3}&attack4=${attack4}`, []);
  }

  updateCharacter(name: string,newName: string, hp: number, defense: number, sprite: string, fullsprite: string, attack1: number, attack2: number, attack3: number, attack4: number) {
    return this.http.put(`${API_PATH}/Admin/AdminUpdateCharacter?name=${name}&newName=${newName}&hp=${hp}&defense=${defense}&sprite=${sprite}&fullsprite=${fullsprite}&attack1=${attack1}&attack2=${attack2}&attack3=${attack3}&attack4=${attack4}`, []);
  }

  deleteCharacter(name: string) {
    return this.http.delete(`${API_PATH}/Admin/AdminDeleteCharacter?name=${name}`);
  }


}
