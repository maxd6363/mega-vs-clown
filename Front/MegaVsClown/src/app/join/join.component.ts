import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../game.service';
import { gameCode, setGameCode, setPlayerName } from '../global';

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css'],
})
export class JoinComponent implements OnInit {
  @Input() error: string = '';
  @Input() code: string = '';
  gameService: GameService;

  constructor(private router: Router, gameService: GameService) {
    this.gameService = gameService;
  }

  ngOnInit(): void {}

  createClicked() {
    this.code = '';
    this.gameService.createGame().subscribe((res) => {
      setGameCode(res);
      this.code = `Le code est : ${gameCode}`;
    });
  }

  joinClicked(code: string) {
    this.code = '';
    setGameCode(code);
    this.gameService.isGameExisting().subscribe((res) => {
      if (res) this.code = `Partie ${gameCode} rejointe`;
      else this.code = `Impossible de rejoindre la partie ${gameCode}`;
    });
  }

  playClicked(name: string) {
    this.error = '';

    if (name !== '') {
      setPlayerName(name);
      this.gameService.addPlayer(name).subscribe(
        (response) => {
          if (response.status == 200) {
            this.router.navigate(['/selection']);
          }
        },
        (err) => {
          this.error = 'Impossible de rejoindre la partie !';
        }
      );
    } else {
      this.error = 'Le pseudo ne doit pas être vide !';
    }
  }
}
