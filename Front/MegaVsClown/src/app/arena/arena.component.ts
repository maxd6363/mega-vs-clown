import { Component, Input, OnInit } from '@angular/core';
import { Observable, interval } from 'rxjs';
import Arena from 'src/models/Arena';
import Attack from 'src/models/Attack';
import Character from 'src/models/Character';
import Player from 'src/models/Player';
import { GameService } from '../game.service';
import {
  deadCharacter,
  loadingAttack,
  loadingCharacter,
  playerName,
  selectedArena,
} from '../global';

@Component({
  selector: 'app-arena',
  templateUrl: './arena.component.html',
  styleUrls: ['./arena.component.css'],
})
export class ArenaComponent implements OnInit {
  @Input() arena: Arena = selectedArena;
  gameService: GameService;
  players: Player[] = [];
  character1: Character = loadingCharacter;
  character2: Character = loadingCharacter;
  attacks: Attack[] = [
    loadingAttack,
    loadingAttack,
    loadingAttack,
    loadingAttack,
  ];
  winner: string = '';
  myIndexInGame: number = 0;

  constructor(gameService: GameService) {
    this.gameService = gameService;
  }

  ngOnInit(): void {
    interval(1000).subscribe((val) => {
      this.getPlayers();
      this.getWinner();
      this.myIndexInGame=this.players.findIndex(x => x.name === playerName);
    });
    this.gameService.startGame().subscribe((x) => this.getPlayers());
  }

  attackClicked(attackName: string) {
    this.gameService.makeMove(attackName).subscribe((x) => this.getPlayers());
  }

  getPlayers() {
    this.gameService.getPlayers().subscribe((players) => {
      this.players = players;
      this.character1 = players[0]?.characters.find((x) => {
        if (players[0].current === null) return false;
        else return x.characterName == players[0].current.characterName;
      })!;
      if (this.character1 === null || !this.players[0].alive)
        this.character1 = deadCharacter;

      this.character2 = players[1]?.characters.find((x) => {
        if (players[1].current === null) return false;
        else return x.characterName == players[1].current.characterName;
      })!;
      if (this.character2 === null || !this.players[1].alive)
        this.character2 = deadCharacter;

      this.attacks = players.find(
        (x) => x.name === playerName
      )?.current.attacks!;
    });
  }

  getWinner() {
    this.gameService.getWinner().subscribe((winner) => (this.winner = winner));
    if (this.winner !== '') {
      console.log('winner : ', this.winner);
    }
  }
}
