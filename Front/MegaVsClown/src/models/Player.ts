import { loadingCharacter } from 'src/app/global';
import Character from './Character';

export default class Player {
  characters: Character[] = [];
  alive: boolean = false;
  ready: boolean = false;
  name: string = '';
  current: Character = loadingCharacter;
  available: Date = new Date();

  constructor() {}
}
