# Mega VS Clown

## Auteurs 

- Margot NOYELLE
- Maxime POULAIN
- Dylan RODRIGUES
- Baptiste VALLET


## Mega VS Clown - Super Smash Bros Like

Le site web est accessible à l'adresse suivante : (sous réserve de coupure d'électricité chez moi -_-)

- API  : http://91.170.211.147:57000/index.html
- Site : http://91.170.211.147:57500


Le site ne marchera pas tout de suite, car la base de données sur Azure se met en veille dès qu'elle n'est plus utilisée.
Il faut environ 4-5 minutes pour qu'elle redémarre. 

### Rejoindre

Le système de partie est géré avec des codes à 3 caractères : 
- Le premier joueur créé une partie, un code lui est affiché.
- Le second joueur copie le code, puis rejoint la partie.
- Les deux joueurs entrent leur pseudo.

### Sélection personnage 

La sélection des personnages : 
- Chaque joueur doit choisir exactement 3 personnages dans une liste de 76 personnages.
- Chaque personnage à une image d'icône et une image pour le sprite en jeu.

### Sélection arène

La sélection de l'arène :
- Chaque joueur sélectionne son arène, elle est juste visuelle et peut être différente pour chaque joueur.
- Les arènes sont animées.

### Partie

La partie :
- Chaque personnage possède 4 attaques différentes, avec une puissance et un cooldown.
- Le premier joueur à éliminer les 3 personnages adverses gagne.

### Adminisatration
Accessible en cliquant sur le bouton _admin_ en haut à droite de la page d'accueil.

- Les personnages s'affichent en page pour ne pas charger trop d'informations d'un coup.
- Toutes les caractéristiques d'un personnage peuvent être changées.

### Visuel

#### Démonstration Vidéo


[![Démonstration](https://i.ibb.co/J3xGmv9/miniature.png)](https://www.youtube.com/embed/OyMMheFSUI0)


#### Screenshots

Menu principale:

<img src="https://i.ibb.co/jD5W7bM/Code.png" />

Menu de séléction des personnages :

<img src="https://i.ibb.co/y8rfxz7/Selection.png" />

Menu de séléction de l'arène :

<img src="https://i.ibb.co/F8fphbx/Arene.png" />

Le combat :

<img src="https://i.ibb.co/zb92C73/Game.png" />

La fin du combat :

<img src="https://i.ibb.co/SnBBh1s/Winner.png" />

La page d'adminisatration :

<img src="https://i.ibb.co/NNYpttQ/Admin.png" />

