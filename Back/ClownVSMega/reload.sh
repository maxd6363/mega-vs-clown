echo $(tput setaf 1)Stopping server
sudo systemctl stop MegaVSClown-Server

echo $(tput setaf 2)Building server
tput sgr0
sudo dotnet publish -c Release -o /srv/MegaVSClown-Server/

sudo systemctl start MegaVSClown-Server
echo $(tput setaf 2)Starting server

journalctl -u MegaVSClown-Server.service -ef

