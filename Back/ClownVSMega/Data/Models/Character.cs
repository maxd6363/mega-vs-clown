﻿using System;
using System.Collections.Generic;

namespace Data.Models
{
    public partial class Character
    {
        public string CharacterName { get; set; } = null!;
        public double Hp { get; set; }
        public double Defense { get; set; }
        public string? Sprite { get; set; }
        public string? FullSprite { get; set; }
        public int? AttackName1 { get; set; }
        public int? AttackName2 { get; set; }
        public int? AttackName3 { get; set; }
        public int? AttackName4 { get; set; }

        public virtual Attack? AttackName1Navigation { get; set; }
        public virtual Attack? AttackName2Navigation { get; set; }
        public virtual Attack? AttackName3Navigation { get; set; }
        public virtual Attack? AttackName4Navigation { get; set; }
    }
}
