﻿using Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public partial class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Arena> Arenas { get; set; } = null!;
        public virtual DbSet<Attack> Attacks { get; set; } = null!;
        public virtual DbSet<Character> Characters { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=tcp:megavsclown.database.windows.net,1433;Initial Catalog=UltimateGameDarkDatabase;Persist Security Info=False;User ID=UltimateRoot;Password=MegaRoot63;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Arena>(entity =>
            {
                entity.HasKey(e => e.ArenaName)
                    .HasName("PK__arena__CA11C6B6EF5174F7");

                entity.ToTable("arena");

                entity.Property(e => e.ArenaName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("arena_name");

                entity.Property(e => e.Sprite)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("sprite");
            });

            modelBuilder.Entity<Attack>(entity =>
            {
                entity.ToTable("attack");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.AttackName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("attack_name");

                entity.Property(e => e.Cooldown).HasColumnName("cooldown");

                entity.Property(e => e.Description)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("description");

                entity.Property(e => e.Power).HasColumnName("power");

                entity.Property(e => e.Precision).HasColumnName("precision");
            });

            modelBuilder.Entity<Character>(entity =>
            {
                entity.HasKey(e => e.CharacterName)
                    .HasName("PK__characte__88C6AF3B944A2FEA");

                entity.ToTable("character");

                entity.Property(e => e.CharacterName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("character_name");

                entity.Property(e => e.AttackName1).HasColumnName("attack_name_1");

                entity.Property(e => e.AttackName2).HasColumnName("attack_name_2");

                entity.Property(e => e.AttackName3).HasColumnName("attack_name_3");

                entity.Property(e => e.AttackName4).HasColumnName("attack_name_4");

                entity.Property(e => e.Defense).HasColumnName("defense");

                entity.Property(e => e.FullSprite)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("full_sprite");

                entity.Property(e => e.Hp).HasColumnName("hp");

                entity.Property(e => e.Sprite)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("sprite");

                entity.HasOne(d => d.AttackName1Navigation)
                    .WithMany(p => p.CharacterAttackName1Navigations)
                    .HasForeignKey(d => d.AttackName1)
                    .HasConstraintName("fk_character_attack1");

                entity.HasOne(d => d.AttackName2Navigation)
                    .WithMany(p => p.CharacterAttackName2Navigations)
                    .HasForeignKey(d => d.AttackName2)
                    .HasConstraintName("fk_character_attack2");

                entity.HasOne(d => d.AttackName3Navigation)
                    .WithMany(p => p.CharacterAttackName3Navigations)
                    .HasForeignKey(d => d.AttackName3)
                    .HasConstraintName("fk_character_attack3");

                entity.HasOne(d => d.AttackName4Navigation)
                    .WithMany(p => p.CharacterAttackName4Navigations)
                    .HasForeignKey(d => d.AttackName4)
                    .HasConstraintName("fk_character_attack4");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
