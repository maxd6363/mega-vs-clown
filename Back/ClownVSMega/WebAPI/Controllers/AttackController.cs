﻿using Data;
using DTO;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AttackController
    {
        private readonly DatabaseContext context;
        public AttackController(DatabaseContext context)
        {
            this.context = context;
        }

        [HttpGet("GetAttacks")]
        public List<AttackDTO> GetAttacks()
        {
            return context.Attacks.ToList().ToDTO();
        }


    }
}
