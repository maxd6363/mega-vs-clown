﻿namespace DTO
{
    public class CharacterDTO
    {
        public CharacterDTO()
        {
            Attacks = new List<AttackDTO>();
        }

        public string CharacterName { get; set; } = null!;
        public double HpMax { get; set; }
        public double Hp { get; set; }
        public double Defense { get; set; }
        public string? Sprite { get; set; }
        public string? FullSprite { get; set; }

        public virtual List<AttackDTO> Attacks { get; set; }
    }
}
