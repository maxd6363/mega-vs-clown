﻿namespace DTO
{
    public class ArenaDTO
    {
        public string ArenaName { get; set; } = null!;
        public string Sprite { get; set; } = null!;
    }
}