﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match
{
    internal class Utils
    {
        public static Random Random { get; }
        private static readonly string chars = "ABCDEFGHJKLMPRSTUWXYZ23456789";


        static Utils()
        {
            Random = new Random();
        }

        public static string RandomString(List<string> codes, int length)
        {
            string code = string.Empty;
            int i = 0;
            do
            {
                code = new string(Enumerable.Repeat(chars, length).Select(s => s[Random.Next(s.Length)]).ToArray());
                i++;
            } while (codes.Contains(code) || i < 50);
            return code;
        }

    }
}
