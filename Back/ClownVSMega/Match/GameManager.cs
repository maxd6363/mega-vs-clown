﻿using DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match
{
    public class GameManager
    {
        public List<Game> Games { get; set; }

        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new GameManager();
                return instance;
            }
        }

        private static GameManager? instance;


        private GameManager()
        {
            Games = new List<Game>();
        }



        public bool Existe(string code)
        {
            var game = Find(code);
            return game != null;
        }

        public string Create()
        {
            var code = Utils.RandomString(Games.Select(x => x.Code).ToList(), 3);
            var game = new Game(code);
            Games.Add(game);
            Console.WriteLine($"Create Game <{code}>");
            return code;
        }

        public bool AddPlayer(string code, string name)
        {
            var game = Find(code);
            if (game == null) return false;
            Console.WriteLine($"Add Player {name}");
            return game.AddPlayer(name);
        }

        public bool AddCharacter(string code, string playerName, CharacterDTO character)
        {
            var game = Find(code);
            if (game == null) return false;
            Player player;
            try
            {
                player = game.Players.First(x => x.Name == playerName);
                if (player == null) return false;
            }
            catch (InvalidOperationException) { return false; }
            if (player.Characters.Count >= 3) return false;
            try
            {
                var contains = player.Characters.First(x => x.CharacterName == character.CharacterName);
                if (contains != null) return false;
            }
            catch (InvalidOperationException) { }
            Console.WriteLine($"Add character {character.CharacterName} to {playerName}");
            player.AddCharacter(character);
            return true;
        }

        public bool MakeMove(string code, string attackerPlayerName, string attackName)
        {
            var game = Find(code);
            if (game == null) return false;
            Console.WriteLine($"Make move from player {attackerPlayerName} with {attackName}");
            return game.MakeMove(attackerPlayerName, attackName);
        }

        public bool Ready(string code)
        {
            var game = Find(code);
            if (game == null) return false;
            return game.Ready;
        }

        public void Start(string code)
        {
            var game = Find(code);
            if (game == null) return;
            game.Start();
        }
        public bool IsStarted(string code)
        {
            var game = Find(code);
            if (game == null) return false;
            return game.Started;
        }

        public void Stop(string code)
        {
            var game = Find(code);
            if (game == null) return;
            Games.Remove(game);
        }

        public IEnumerable<Player> GetPlayers(string code)
        {
            var game = Find(code);
            if (game == null) return Enumerable.Empty<Player>();
            return game.Players;
        }

        private Game Find(string code)
        {
            return Games.Find(x => x.Code.ToUpper() == code.ToUpper());
        }

        public string GetWinner(string code)
        {
            var game = Find(code);
            if (game == null) return string.Empty;
            if (game.Players.Any(x => !x.Alive))
            {
                var winner = game.Players.FirstOrDefault(x => x.Alive);
                return winner == null ? string.Empty : winner.Name;
            }
            return string.Empty;
        }
    }
}
